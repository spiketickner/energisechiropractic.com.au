<?php 
/**
 * @package 	WordPress
 * @subpackage 	Luxury Spa
 * @version		1.0.0
 * 
 * Footer Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_option = luxury_spa_get_global_options();
?>
<div class="footer <?php echo 'cmsmasters_color_scheme_' . $cmsmasters_option['luxury-spa' . '_footer_scheme'] . ($cmsmasters_option['luxury-spa' . '_footer_type'] == 'default' ? ' cmsmasters_footer_default' : ' cmsmasters_footer_small'); ?>">
	<div class="footer_inner">
		<?php 
		if (
			$cmsmasters_option['luxury-spa' . '_footer_type'] == 'default' && 
			$cmsmasters_option['luxury-spa' . '_footer_logo']
		) {
			luxury_spa_footer_logo($cmsmasters_option);
		}
		
		
		if (
			(
				$cmsmasters_option['luxury-spa' . '_footer_type'] == 'default' && 
				$cmsmasters_option['luxury-spa' . '_footer_html'] !== ''
			) || (
				$cmsmasters_option['luxury-spa' . '_footer_type'] == 'small' && 
				$cmsmasters_option['luxury-spa' . '_footer_additional_content'] == 'text' && 
				$cmsmasters_option['luxury-spa' . '_footer_html'] !== ''
			)
		) {
			echo '<div class="footer_custom_html_wrap">' . 
				'<div class="footer_custom_html">' . 
					do_shortcode(stripslashes($cmsmasters_option['luxury-spa' . '_footer_html'])) . 
				'</div>' . 
			'</div>';
		}
		
		
		if (
			has_nav_menu('footer') && 
			(
				(
					$cmsmasters_option['luxury-spa' . '_footer_type'] == 'default' && 
					$cmsmasters_option['luxury-spa' . '_footer_nav']
				) || (
					$cmsmasters_option['luxury-spa' . '_footer_type'] == 'small' && 
					$cmsmasters_option['luxury-spa' . '_footer_additional_content'] == 'nav'
				)
			)
		) {
			echo '<div class="footer_nav_wrap">' . 
				'<nav>';
				
				
				wp_nav_menu(array( 
					'theme_location' => 'footer', 
					'menu_id' => 'footer_nav', 
					'menu_class' => 'footer_nav' 
				));
				
				
				echo '</nav>' . 
			'</div>';
		}
		
		
		if (
			isset($cmsmasters_option['luxury-spa' . '_social_icons']) && 
			(
				(
					$cmsmasters_option['luxury-spa' . '_footer_type'] == 'default' && 
					$cmsmasters_option['luxury-spa' . '_footer_social']
				) || (
					$cmsmasters_option['luxury-spa' . '_footer_type'] == 'small' && 
					$cmsmasters_option['luxury-spa' . '_footer_additional_content'] == 'social'
				)
			)
		) {
			luxury_spa_social_icons();
		}
		?>
		<span class="footer_copyright copyright">Energise Chiropractic 2018 | All Rights Reserved <a class="site-footer__rb" href="http://rb.com.au" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="21" height="13.99" viewBox="0 0 21 13.99"><title>Web Design Newcastle - Redback Solutions</title><path d="M2.84,5.71c0-.53,0-1.49-0.08-1.72H0C0,4.83,0,5.68,0,6.66v7.11H2.84V9.5C2.84,6.83,4,6.3,6.08,6.36V3.77A3.27,3.27,0,0,0,2.84,5.71ZM13,3.77A3.21,3.21,0,0,0,10.11,5V0H7.3V10.58c0,1.06,0,2.13,0,3.19H10a4.28,4.28,0,0,0,.12-0.92A2.77,2.77,0,0,0,12.73,14c2.91,0,4.4-2.49,4.4-5.28S15.76,3.77,13,3.77Zm-0.9,8.07c-1.62,0-2.13-1.26-2.13-2.9,0-2,.57-3,2.17-3S14.3,7.13,14.3,8.77C14.3,10.62,13.53,11.84,12.11,11.84ZM19.54,11A1.41,1.41,0,1,0,21,12.36,1.43,1.43,0,0,0,19.54,11Z" transform="translate(0)"></path></svg></a></span>
	</div>
</div>